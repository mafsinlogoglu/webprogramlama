﻿using Computech0._1.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Computech0._1.DAL
{
    public class cmptContext : DbContext
    {
        public cmptContext() : base("cmptContext")
        { }

        public DbSet<Kullanici> Kullanicilar { get; set; }
      
    }
}

