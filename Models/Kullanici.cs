﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Computech0._1.Models
{
    public class Kullanici
    {
        public int ID { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string KullaniciAdi { get; set; }
        public string Email { get; set; }

    }
}