﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Computech0._1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Teknoloji()
        {
            ViewBag.Massage = "Teknoloji sayfası";

            return View();
        }
        public ActionResult Inceleme()
        {
            ViewBag.Massage = "İnceleme sayfası";

            return View();
        }

        public ActionResult SosyalMedya()
        {
            ViewBag.Massage = "Sosyal Medya sayfası";

            return View();
        }

    }
}